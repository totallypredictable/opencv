import cv2
import numpy as np
import datetime

cap = cv2.VideoCapture(0);

while(True):
    ret, frame = cap.read()
    datet = str(datetime.datetime.now())
    font = cv2.FONT_HERSHEY_COMPLEX

    frame = cv2.putText(frame, datet, (10, 50), font, 1,
                        (0, 255, 255), 2, cv2.LINE_AA)
    arry = np.array(frame)
    print(arry.shape)
    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
